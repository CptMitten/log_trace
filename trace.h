#ifndef _IO_TRACE_H
#define _IO_TRACE_H

#include <stdio.h>
#include <ctype.h>
#include <stdbool.h>

#include <libgen.h>

// NDEBUG could be set to any value, this macro guarentee to hold 1 or 0.
#if NDEBUG
#	define TRACE_ON 0
#else
#	define TRACE_ON 1
#endif /* NDEBUG */

/** 
 * @brief Stringification.
 * First expand __LINE__ like macros before the stringification, very usefull
 * for decimal constant since their type depend on their value. 
 */
#define STRGY(X) STRINGIFY(X)
#define STRINGIFY(X) #X

/**
 * @brief Extract first argument from a list of arguments.
 * 
 * Add a fake argument (0) to make sure macro specific token "..." work with
 * both one or more arguments. Then this new argument is ignored.
 * @see EXTRACT_FIRST0, TRACE
 */
#define EXTRACT_FIRST(...) EXTRACT_FIRST0(__VA_ARGS__, 0)
#define EXTRACT_FIRST0(_0, ...) _0

/**
 * @brief Extract all arguments except the first one from a list of arguments.
 * 
 * Add a fake argument (0) to make sure macro specific token "..." always
 * work. Only suitable in our context, since this artificial last argument will
 * be returned.
 * @see EXTRACT_LAST0, TRACE
 */
#define EXTRACT_LAST(...) EXTRACT_LAST0(__VA_ARGS__, 0)
#define EXTRACT_LAST0(_0, ...) __VA_ARGS__


/**
 * @brief Add textual representation of each arg at the end.
 *
 * Also ignore EXTRACT_LAST fake argument (0) with a "%.0d" format specifier.
 * @see TRACE_PRINT, EXTRACT_FIRST, EXTRACT_LAST, trace_skip
 */
#define TRACE(...)                                                             \
        TRACE_PRINT(EXTRACT_FIRST(__VA_ARGS__) "%.0d (%s)",                    \
                    EXTRACT_LAST(__VA_ARGS__), trace_skip(#__VA_ARGS__))
/** 
 * @brief Print a message to stderr and add additional informations.
 * 
 * This function print file name, line number, function name.
 * @param F Format string that specifies how subsequent argument are converted.
 * @param ... Variable length argument list.
 */
#define TRACE_PRINT(F, ...)                                                    \
        do {                                                                   \
        	if (TRACE_ON) {                                                    \
        		fprintf(stderr, "File <%s>, line " STRGY(__LINE__)             \
        		        ", in function \"%s\":\n\t" F ".\n",                   \
        		        basename(__FILE__), __func__, __VA_ARGS__);            \
        	}                                                                  \
        } while(false)

/** 
 * @brief Return textual representation of subsequent arguments. 
 * 
 * Skip the format string in @a expr to return only the subsequent arguments 
 * to be converted by the format string.
 * @param expr Textual representation of __VA_ARGS__.
 * @see TRACE
 */
static inline const char *trace_skip(const char expr[static 1])
{
    int count = 0;
    for (size_t i = 0; expr[i] != '\0'; i++) {
        if (expr[i] == '"') {
            count++;
        } else if (count >= 2 && isalpha(expr[i])) {
            return &expr[i];
        }
    }
    return "";
}

#endif /* _IO_TRACE_H */